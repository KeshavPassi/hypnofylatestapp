package com.hypnofy.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;
import com.hypnofy.utils.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MaleAndFemaleActivity extends BaseActivity {
    Activity mActivity = MaleAndFemaleActivity.this;
    String TAG = MaleAndFemaleActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.imgbackIV)
    ImageView imgbackIV;

    @BindView(R.id.welcomTExt)
    TextView welcomTExt;
    @BindView(R.id.txtmaleLL)
    LinearLayout txtmaleLL;
    @BindView(R.id.txtfemaleLL)
    LinearLayout txtfemaleLL;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.BCKRL)
    RelativeLayout BCKRL;


    String strType = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_male_and_female);
        ButterKnife.bind(this);
//        welcomTExt.setText("What would you"+"\n" +"like help with?");
//        welcomTExt.setText("How can we "+"\n"+"help you?");
        welcomTExt.setText("Would you prefer male"+"\n"+" or female voice?");
    }
    @OnClick({R.id.imgCloseIV, R.id.txtmaleLL, R.id.imgbackIV, R.id.backRL, R.id.BCKRL, R.id.txtfemaleLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCloseIV:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
                case R.id.backRL:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
            case R.id.imgbackIV:
                Intent mIntent5 = new Intent(mActivity,AnxticttyActivity.class);
                startActivity(mIntent5);
                finish();
                break;
                case R.id.BCKRL:
                Intent mIntent25 = new Intent(mActivity,AnxticttyActivity.class);
                startActivity(mIntent25);
                finish();
                break;
            case R.id.txtmaleLL:
                Constants.strGender="male";
                txtmaleLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtfemaleLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent4 = new Intent(mActivity,PleaseTellUsActivity.class);
                startActivity(mIntent4);
                finish();
                break;
            case R.id.txtfemaleLL:
                Constants.strGender="female";
                txtfemaleLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtmaleLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent = new Intent(mActivity,PleaseTellUsActivity.class);
                startActivity(mIntent);
                finish();
                break;

//            case R.id.txtBeachInduction:
//                Intent mIntent1 = new Intent(mActivity,GreatePlayActivity.class);
//                mIntent1.putExtra("TYPE", txtBeachInduction.getText().toString().trim());
//                startActivity(mIntent1);
//                finish();
//                break;
//            case R.id.txtFullBodyRelaxTV:
//                Intent mIntent2 = new Intent(mActivity,GreatePlayActivity.class);
//                mIntent2.putExtra("TYPE", txtFullBodyRelaxTV.getText().toString().trim());
//                startActivity(mIntent2);
//                finish();
//                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent5 = new Intent(mActivity,AnxticttyActivity.class);
        startActivity(mIntent5);
        finish();
    }
}
