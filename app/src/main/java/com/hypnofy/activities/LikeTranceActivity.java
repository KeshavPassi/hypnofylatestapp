package com.hypnofy.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LikeTranceActivity extends BaseActivity {
    Activity mActivity = LikeTranceActivity.this;
    String TAG = LikeTranceActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.imgbackIV)
    ImageView imgbackIV;

    @BindView(R.id.welcomTExt)
    TextView welcomTExt;
    @BindView(R.id.txtAnxityLL)
    LinearLayout txtAnxityLL;
    @BindView(R.id.txtLossWeightLL)
    LinearLayout txtLossWeightLL;
    @BindView(R.id.txtStopSmokingLL)
    LinearLayout txtStopSmokingLL;
    @BindView(R.id.txtStressReductioLL)
    LinearLayout txtStressReductioLL;
    @BindView(R.id.txthealthierLL)
    LinearLayout txthealthierLL;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.BCKRL)
    RelativeLayout BCKRL;

    String strType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like_trance);
        ButterKnife.bind(this);
//        welcomTExt.setText("What would you"+"\n" +"like help with?");
//        welcomTExt.setText("How can we "+"\n"+"help you?");
        welcomTExt.setText("How would you " + "\n" + "like to begin?");
    }

    @OnClick({R.id.imgCloseIV, R.id.txtAnxityLL, R.id.txthealthierLL, R.id.imgbackIV, R.id.BCKRL, R.id.backRL, R.id.txtLossWeightLL, R.id.txtStopSmokingLL, R.id.txtStressReductioLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCloseIV:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
            case R.id.backRL:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
            case R.id.imgbackIV:
                Intent mIntent5 = new Intent(mActivity, LikeHelpActivity.class);
                startActivity(mIntent5);
                finish();
                break;
            case R.id.BCKRL:
                Intent mIntent50 = new Intent(mActivity, LikeHelpActivity.class);
                startActivity(mIntent50);
                finish();
                break;
            case R.id.txtAnxityLL:
                txtAnxityLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtLossWeightLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtStopSmokingLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtStressReductioLL.setBackgroundColor(getResources().getColor(R.color.white));
                txthealthierLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent4 = new Intent(mActivity, AnxticttyActivity.class);
                startActivity(mIntent4);
                finish();
                break;
            case R.id.txtLossWeightLL:
                txtLossWeightLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtStopSmokingLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtStressReductioLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtAnxityLL.setBackgroundColor(getResources().getColor(R.color.white));
                txthealthierLL.setBackgroundColor(getResources().getColor(R.color.white));
                Toast.makeText(mActivity, getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
            case R.id.txtStopSmokingLL:
                txtStopSmokingLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtLossWeightLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtStressReductioLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtAnxityLL.setBackgroundColor(getResources().getColor(R.color.white));
                txthealthierLL.setBackgroundColor(getResources().getColor(R.color.white));
                Toast.makeText(mActivity, getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
            case R.id.txtStressReductioLL:
                txtStressReductioLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtStopSmokingLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtLossWeightLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtAnxityLL.setBackgroundColor(getResources().getColor(R.color.white));
                txthealthierLL.setBackgroundColor(getResources().getColor(R.color.white));
                Toast.makeText(mActivity, getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
            case R.id.txthealthierLL:
                txthealthierLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtStopSmokingLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtLossWeightLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtAnxityLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtStressReductioLL.setBackgroundColor(getResources().getColor(R.color.white));
                Toast.makeText(mActivity, getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
//            case R.id.txtBeachInduction:
//                Intent mIntent1 = new Intent(mActivity,GreatePlayActivity.class);
//                mIntent1.putExtra("TYPE", txtBeachInduction.getText().toString().trim());
//                startActivity(mIntent1);
//                finish();
//                break;
//            case R.id.txtFullBodyRelaxTV:
//                Intent mIntent2 = new Intent(mActivity,GreatePlayActivity.class);
//                mIntent2.putExtra("TYPE", txtFullBodyRelaxTV.getText().toString().trim());
//                startActivity(mIntent2);
//                finish();
//                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent5 = new Intent(mActivity, LikeHelpActivity.class);
        startActivity(mIntent5);
        finish();
    }
}
