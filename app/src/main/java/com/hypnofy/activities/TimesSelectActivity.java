package com.hypnofy.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TimesSelectActivity extends BaseActivity {
    Activity mActivity = TimesSelectActivity.this;
    String TAG = TimesSelectActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.imgbackIV)
    ImageView imgbackIV;

    @BindView(R.id.welcomTExt)
    TextView welcomTExt;
    @BindView(R.id.txtOneLL)
    LinearLayout txtOneLL;
    @BindView(R.id.txttwoLL)
    LinearLayout txttwoLL;
    @BindView(R.id.txtthreeLL)
    LinearLayout txtthreeLL;


    String strType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_times_select);
        ButterKnife.bind(this);

        welcomTExt.setText("How much time " + "\n" + "do you have?");
    }

    @OnClick({R.id.imgCloseIV, R.id.txtOneLL, R.id.imgbackIV, R.id.txttwoLL, R.id.txtthreeLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCloseIV:
                onBackPressed();
                break;
            case R.id.imgbackIV:
                Intent mIntent5 = new Intent(mActivity, MaleAndFemaleActivity.class);
                startActivity(mIntent5);
                finish();
                break;
            case R.id.txtOneLL:
                txtOneLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txttwoLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtthreeLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent4 = new Intent(mActivity, GreatePlayActivity.class);
                startActivity(mIntent4);
                finish();
                break;
            case R.id.txttwoLL:
                txttwoLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtOneLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtthreeLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent = new Intent(mActivity, GreatePlayActivity.class);
                startActivity(mIntent);
                finish();
                break;
            case R.id.txtthreeLL:
                txtthreeLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txttwoLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtOneLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent1 = new Intent(mActivity, GreatePlayActivity.class);
                startActivity(mIntent1);
                finish();
                break;

//            case R.id.txtBeachInduction:
//                Intent mIntent1 = new Intent(mActivity,GreatePlayActivity.class);
//                mIntent1.putExtra("TYPE", txtBeachInduction.getText().toString().trim());
//                startActivity(mIntent1);
//                finish();
//                break;
//            case R.id.txtFullBodyRelaxTV:
//                Intent mIntent2 = new Intent(mActivity,GreatePlayActivity.class);
//                mIntent2.putExtra("TYPE", txtFullBodyRelaxTV.getText().toString().trim());
//                startActivity(mIntent2);
//                finish();
//                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
