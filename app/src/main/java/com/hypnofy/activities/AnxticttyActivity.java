package com.hypnofy.activities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AnxticttyActivity extends BaseActivity {
    Activity mActivity = AnxticttyActivity.this;
    String TAG = AnxticttyActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.imgbackIV)
    ImageView imgbackIV;

    @BindView(R.id.welcomTExt)
    TextView welcomTExt;
    @BindView(R.id.txtBeachInduction)
    TextView txtBeachInduction;
    @BindView(R.id.txtFullBodyRelaxTV)
    TextView txtFullBodyRelaxTV;
    @BindView(R.id.txtFullBodyRelaxLL)
    LinearLayout txtFullBodyRelaxLL;
    @BindView(R.id.txtBeachInductionLL)
    LinearLayout txtBeachInductionLL;
    @BindView(R.id.txtforest_walkLL)
    LinearLayout txtforest_walkLL;
    @BindView(R.id.BCKRL)
    RelativeLayout BCKRL;
 @BindView(R.id.backRL)
    RelativeLayout backRL;

    String strType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anxtictty);
        ButterKnife.bind(this);

        welcomTExt.setText("How would you like"+"\n"+ "to get into trance?");
    }

    @OnClick({R.id.imgCloseIV, R.id.txtFullBodyRelaxLL, R.id.imgbackIV, R.id.backRL, R.id.txtBeachInductionLL, R.id.txtforest_walkLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;  case R.id.imgCloseIV:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
                case R.id.imgbackIV:

                Intent mIntent3 = new Intent(mActivity,LikeTranceActivity.class);
                startActivity(mIntent3);
                finish();

                break;
                case R.id.BCKRL:
                    Intent mIntent13 = new Intent(mActivity,LikeTranceActivity.class);
                    startActivity(mIntent13);
                    finish();

                break;
            case R.id.txtFullBodyRelaxLL:
                txtFullBodyRelaxLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtBeachInductionLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtforest_walkLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent2 = new Intent(mActivity,MaleAndFemaleActivity.class);
                mIntent2.putExtra("TYPE", txtFullBodyRelaxTV.getText().toString().trim());
                startActivity(mIntent2);
                finish();
                break;
            case R.id.txtBeachInductionLL:
                txtBeachInductionLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtFullBodyRelaxLL.setBackgroundColor(getResources().getColor(R.color.white));
                txtforest_walkLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent1 = new Intent(mActivity,MaleAndFemaleActivity.class);
                mIntent1.putExtra("TYPE", txtBeachInduction.getText().toString().trim());
                startActivity(mIntent1);
                finish();
                break;
                case R.id.txtforest_walkLL:
                    txtforest_walkLL.setBackgroundColor(getResources().getColor(R.color.txt_colr_home));
                txtFullBodyRelaxLL.setBackgroundColor(getResources().getColor(R.color.white));
                    txtBeachInductionLL.setBackgroundColor(getResources().getColor(R.color.white));
                Intent mIntent4 = new Intent(mActivity,MaleAndFemaleActivity.class);

                startActivity(mIntent4);
                finish();
                break;


        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent3 = new Intent(mActivity,LikeTranceActivity.class);
        startActivity(mIntent3);
        finish();
    }
}
