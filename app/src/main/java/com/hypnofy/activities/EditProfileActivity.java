package com.hypnofy.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hypnofy.BaseActivity;
import com.hypnofy.R;
import com.hypnofy.beans.EditProfileResponse;
import com.hypnofy.beans.SignUpReponse;
import com.hypnofy.hypnofyApplication;
import com.hypnofy.retrofile.RestClient;
import com.hypnofy.utils.AlertDialogManager;
import com.hypnofy.utils.Constants;
import com.hypnofy.utils.Utilities;
import com.hypnofy.utils.hypnofyPreference;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class EditProfileActivity extends BaseActivity {
    public final int CAMERA_REQUEST = 333;
    public final int GALLERY_REQUEST = 222;
    public final int REQUEST_PERMISSION_CODE = 919;
    public Bitmap mBitmapImage;
    public String mPictureType = "";
    String mCurrentPhotoPath, mStoragePath = "";
    private String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String writeCamera = Manifest.permission.CAMERA;

    Activity mActivity = EditProfileActivity.this;
    String TAG = EditProfileActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.imgProfileEditIV)
    CircleImageView imgProfileEditIV;
    @BindView(R.id.editFirstNameET)
    EditText editFirstNameET;
    @BindView(R.id.editLastNameET)
    EditText editLastNameET;
    @BindView(R.id.btnSave)
    TextView btnSave;
    @BindView(R.id.BackLL)
    RelativeLayout BackLL;
    static String strImageBase64 = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        setData();
    }

    private void setData() {
        strImageBase64 = hypnofyPreference.readString(mActivity, hypnofyPreference.USER_IMAGE, "");
//      if(!strImageBase64.contains("graph")){
        if (!strImageBase64.equals("")) {
            Glide.with(mActivity)
                    .load(strImageBase64)
                    .asBitmap()
                    .placeholder(R.drawable.placeholder)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            // you can do something with loaded bitmap here
                            imgProfilePicCIV.setImageBitmap(resource);
                        }
                    });
        }
        editFirstNameET.setText(hypnofyPreference.readString(mActivity, hypnofyPreference.FIRST_NAME, ""));
        editFirstNameET.setSelection(hypnofyPreference.readString(mActivity, hypnofyPreference.FIRST_NAME, "").length());
        editLastNameET.setText(hypnofyPreference.readString(mActivity, hypnofyPreference.LAST_NAME, ""));
        editLastNameET.setSelection(hypnofyPreference.readString(mActivity, hypnofyPreference.LAST_NAME, "").length());
    }


    @OnClick({R.id.imgProfileEditIV, R.id.btnSave, R.id.BackLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgProfileEditIV:
                setUpCameraGallery();
                break;
            case R.id.btnSave:
                if (editFirstNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_first));
                } else if (editLastNameET.getText().toString().trim().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_last));
                }else {
                    if (Utilities.isNetworkAvailable(mActivity)) {
                        String strUSERID = hypnofyPreference.readString(mActivity, hypnofyPreference.USER_ID, "");
                        executeAPI();
                    } else {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.internetconnection));
                    }
                }
                break;
            case R.id.BackLL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Constants.IsEditProfile = true;
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        startActivity(intent);

        finish();
    }

    /*Getting Profile Pic*/
    private void setUpCameraGallery() {
        if (checkPermission())
            openCameraGalleryDialog();
        else
            requestPermission();
    }

    public void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(mActivity, "com.hypnofy.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    private void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, REQUEST_PERMISSION_CODE);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    openCameraGalleryDialog();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission();
                }
                return;
            }

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        InputStream stream = null;
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            File finalFile = new File(getRealPathFromURI(uri));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 0;

            Bitmap bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
            ExifInterface exifInterface = null;
            try {
                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                default:

            }
            mBitmapImage = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            imgProfilePicCIV.setImageBitmap(mBitmapImage);
            strImageBase64 = getBase64String(mBitmapImage);
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            Bitmap bitmap = BitmapFactory.decodeFile(mStoragePath, options);
            Bitmap thumb1 = Utilities.rotateImage(bitmap, mStoragePath);

            mBitmapImage = thumb1;
            imgProfilePicCIV.setImageBitmap(mBitmapImage);
            strImageBase64 = getBase64String(mBitmapImage);

        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void executeAPI() {
        showProgressDialog(mActivity);
        Map<String, String> data = new HashMap<>();
        data.put("user_image", strImageBase64);
        data.put("user_id", hypnofyPreference.readString(mActivity, hypnofyPreference.USER_ID, ""));
        data.put("last_name", editLastNameET.getText().toString());
        data.put("first_name", editFirstNameET.getText().toString());
        RestClient.get().editProfile(data).enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(1)) {
                        showAlertDialog(getString(R.string.app_name), response.body().getMessage());
                    } else if (response.body().getStatus().equals(0)) {
                        AlertDialogManager.showAlertDialog(mActivity,"Error!",response.body().getMessage());
//                        Toast.makeText(mActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mActivity, "Error temporary unavailble", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(mActivity, "Error temporary unavailble", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "====ERROR===");
            }
        });
    }

    public String resizeBase64Image(String base64image, int IMG_WIDTH, int IMG_HEIGHT) {
        byte[] encodeByte = Base64.decode(base64image.getBytes(), Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPurgeable = true;
        Bitmap image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length, options);
        if (image.getHeight() <= 300 && image.getWidth() <= 300) {
            return base64image;
        }
        image = Bitmap.createScaledBitmap(image, IMG_WIDTH, IMG_HEIGHT, false);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        System.gc();
        return Base64.encodeToString(b, Base64.NO_WRAP);
    }

    private String getBase64String(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] imageBytes = baos.toByteArray();

        String base64String = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return base64String;
    }

    public void showAlertDialog(String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitleTV = (TextView) alertDialog.findViewById(R.id.txtTitleTV);
        TextView txtMessageTV = (TextView) alertDialog.findViewById(R.id.txtMessageTV);
        Button btnDismiss = (Button) alertDialog.findViewById(R.id.btnDismiss);

        txtTitleTV.setText(strTitle);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.IsEditProfile = true;
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent);

                finish();
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }

}
