package com.hypnofy.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LikeHelpActivity extends BaseActivity {
    Activity mActivity = LikeHelpActivity.this;
    String TAG = LikeHelpActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;


    @BindView(R.id.txtAnxityTV)
    TextView txtAnxityTV;
    @BindView(R.id.mainLL)
    RelativeLayout mainLL;
    @BindView(R.id.backRL)
    RelativeLayout backRL;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_like_help);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.imgCloseIV, R.id.mainLL, R.id.txtAnxityTV, R.id.backRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCloseIV:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
            case R.id.backRL:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;

            case R.id.txtAnxityTV:
                Intent mIntent3 = new Intent(mActivity, LikeTranceActivity.class);
                mIntent3.putExtra("TYPE", txtAnxityTV.getText().toString().trim());
                startActivity(mIntent3);
                finish();
                break;
            case R.id.mainLL:
                Intent mIntent4 = new Intent(mActivity, LikeTranceActivity.class);
                mIntent4.putExtra("TYPE", txtAnxityTV.getText().toString().trim());
                startActivity(mIntent4);
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mActivity, HomeActivity.class));
        finish();
    }
}
