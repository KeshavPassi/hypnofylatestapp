package com.hypnofy.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hypnofy.R;
import com.hypnofy.beans.SignUpReponse;
import com.hypnofy.retrofile.RestClient;
import com.hypnofy.utils.AlertDialogManager;
import com.hypnofy.utils.Utilities;
import com.hypnofy.utils.hypnofyPreference;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {
    public final int CAMERA_REQUEST = 333;
    public final int GALLERY_REQUEST = 222;
    public final int REQUEST_PERMISSION_CODE = 919;
    public Bitmap mBitmapImage;
    public String mPictureType = "";
    Activity mActivity = SignUpActivity.this;
    String TAG = SignUpActivity.this.getClass().getSimpleName();
    String mCurrentPhotoPath, mStoragePath = "", strImageBase64 = "";
    @BindView(R.id.editFirstNameET)
    EditText editFirstNameET;
    @BindView(R.id.editLastNameET)
    EditText editLastNameET;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.btnSignUp)
    Button btnSignUp;
    @BindView(R.id.txtAlreadyMemberTV)
    TextView txtAlreadyMemberTV;
    @BindView(R.id.backLLSU)
    LinearLayout backLLSU;
    @BindView(R.id.txtTermsConditionsTV)
    TextView txtTermsConditionsTV;
    @BindView(R.id.imgProfilePicCIV)
    CircleImageView imgProfilePicCIV;
    @BindView(R.id.imgIV)
    ImageView imgIV;
    @BindView(R.id.termsAndConditionRL)
    RelativeLayout termsAndConditionRL;
    private String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String writeCamera = Manifest.permission.CAMERA;
    boolean isCheck = true;
    String strCheckstatus ="0";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnSignUp, R.id.txtAlreadyMemberTV, R.id.backLLSU, R.id.termsAndConditionRL, R.id.txtTermsConditionsTV, R.id.imgProfilePicCIV, R.id.imgIV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnSignUp:
                validate();
                break;
            case R.id.txtAlreadyMemberTV:
                startActivity(new Intent(mActivity, LoginActivity.class));
                finish();
                break;
            case R.id.backLLSU:
                onBackPressed();
                break;
            case R.id.txtTermsConditionsTV:
                if (isCheck) {
                    isCheck = false;
                    strCheckstatus ="0";
                    imgIV.setImageDrawable(getResources().getDrawable(R.drawable.uncheck_icon));
                } else {
                    isCheck = true;
                    strCheckstatus ="1";
                    imgIV.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
                }
                break;
                case R.id.termsAndConditionRL:
                if (isCheck) {
                    isCheck = false;
                    strCheckstatus ="0";
                    imgIV.setImageDrawable(getResources().getDrawable(R.drawable.uncheck_icon));
                } else {
                    isCheck = true;
                    strCheckstatus ="1";
                    imgIV.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
                }
                break;
            case R.id.imgProfilePicCIV:
                /*Open Camera/Gallery*/
                setUpCameraGallery();
                case R.id.imgIV:
                    if (isCheck) {
                        isCheck = false;
                        strCheckstatus ="0";
                        imgIV.setImageDrawable(getResources().getDrawable(R.drawable.uncheck_icon));
                    } else {
                        isCheck = true;
                        strCheckstatus ="1";
                        imgIV.setImageDrawable(getResources().getDrawable(R.drawable.check_icon));
                    }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity,SelectionActivity.class));
        finish();
    }

    private void validate() {
        if (editFirstNameET.getText().toString().trim().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_first));
        } else if (editLastNameET.getText().toString().trim().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_last));
        } else if (editEmailET.getText().toString().trim().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_email));
        } else if (!Utilities.isValidEmaillId(editEmailET.getText().toString())) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_valid_email));
        } else if (editPasswordET.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_password));
        }else if (strCheckstatus.equals("0")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_agree_terms_and_conditions));
        } else {
            //Execute Api
            if (Utilities.isNetworkAvailable(mActivity)) {
                String strFirstName = editFirstNameET.getText().toString().trim();
                String strLastName = editLastNameET.getText().toString().trim();
                String strEmail = editEmailET.getText().toString().trim();
                String strPassword = editPasswordET.getText().toString().trim();
                executeAPI(strFirstName, strLastName, strEmail, strPassword,strCheckstatus);
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.internetconnection));
            }
        }
    }

    private void executeAPI(String fname, String lname, String email, String password,String strCheckstatus) {
        AlertDialogManager.showProgressDialog(mActivity);
        RestClient.get().signup(fname, lname, email, password,strCheckstatus).enqueue(new Callback<SignUpReponse>() {
            @Override
            public void onResponse(Call<SignUpReponse> call, Response<SignUpReponse> response) {
                AlertDialogManager.hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(1)) {
                        Toast.makeText(mActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        hypnofyPreference.writeBoolean(mActivity, hypnofyPreference.IS_LOGIN, true);
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.USER_ID, response.body().getData().getId());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.EMAIL, response.body().getData().getEmail());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.USER_IMAGE, response.body().getData().getUser_image());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.FIRST_NAME, response.body().getData().getFirst_name());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.LAST_NAME, response.body().getData().getLast_name());
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else if (response.body().getStatus().equals(0)) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<SignUpReponse> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "====ERROR===");
            }
        });
    }

    /*Getting Profile Pic*/
    private void setUpCameraGallery() {
        if (checkPermission())
            openCameraGalleryDialog();
        else
            requestPermission();
    }

    public void openCameraGalleryDialog() {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.image_display_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        TextView text_camra = (TextView) dialog.findViewById(R.id.txt_camra);
        TextView text_gallery = (TextView) dialog.findViewById(R.id.txt_gallery);
        TextView txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);

        text_camra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openCamera();
            }
        });
        text_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                openGallery();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Log.e("error is occured", dialog.toString());
    }

    private void openCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri imageURI = FileProvider.getUriForFile(mActivity, "com.hypnofy.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        imageURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        mStoragePath = image.getAbsolutePath();
        return image;
    }

    private void openGallery() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, GALLERY_REQUEST);
    }

    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, REQUEST_PERMISSION_CODE);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    openCameraGalleryDialog();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //requestPermission();
                }
                return;
            }

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        InputStream stream = null;
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            File finalFile = new File(getRealPathFromURI(uri));
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 0;

            Bitmap bitmap = BitmapFactory.decodeFile(finalFile.getAbsolutePath(), options);
            ExifInterface exifInterface = null;
            try {
                exifInterface = new ExifInterface(finalFile.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
            }

            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.setRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.setRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.setRotate(270);
                    break;
                case ExifInterface.ORIENTATION_NORMAL:
                default:

            }
            mBitmapImage = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            imgProfilePicCIV.setImageBitmap(mBitmapImage);
        }
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            Bitmap bitmap = BitmapFactory.decodeFile(mStoragePath, options);
            Bitmap thumb1 = Utilities.rotateImage(bitmap, mStoragePath);

            mBitmapImage = thumb1;
            imgProfilePicCIV.setImageBitmap(mBitmapImage);
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }


}
