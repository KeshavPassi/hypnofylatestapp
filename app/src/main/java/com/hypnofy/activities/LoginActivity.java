package com.hypnofy.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;
import com.hypnofy.beans.LoginReponse;
import com.hypnofy.retrofile.RestClient;
import com.hypnofy.utils.AlertDialogManager;
import com.hypnofy.utils.Utilities;
import com.hypnofy.utils.hypnofyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {
    Activity mActivity = LoginActivity.this;
    String TAG = LoginActivity.this.getClass().getSimpleName();
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.btnLogin)
    TextView btnLogin;
    @BindView(R.id.txtForgotPwdTV)
    TextView txtForgotPwdTV;
    @BindView(R.id.txtDontHaveACTV)
    TextView txtDontHaveACTV;
    @BindView(R.id.backLL)
    LinearLayout backLL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnLogin, R.id.txtForgotPwdTV, R.id.txtDontHaveACTV,R.id.backLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                validate();
                break;
            case R.id.txtForgotPwdTV:
                startActivity(new Intent(mActivity,ForgotPwdActivity.class));
                break;
            case R.id.txtDontHaveACTV:
                startActivity(new Intent(mActivity,SignUpActivity.class));
                finish();
                break;
            case R.id.backLL:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity,SelectionActivity.class));
        finish();
    }

    private void validate() {
        if (editEmailET.getText().toString().trim().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_email));
        } else if (!Utilities.isValidEmaillId(editEmailET.getText().toString())) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_valid_email));
        } else if (editPasswordET.getText().toString().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_password));
        } else {
            //Execute Api
            if (Utilities.isNetworkAvailable(mActivity)) {
                String email = editEmailET.getText().toString().trim();
                String password = editPasswordET.getText().toString().trim();
                executeAPI(email,password,"1");
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.internetconnection));
            }
        }
    }

    private void executeAPI(String email, String password,String login) {
        AlertDialogManager.showProgressDialog(mActivity);
        RestClient.get().login(email,password).enqueue(new Callback<LoginReponse>() {
            @Override
            public void onResponse(Call<LoginReponse> call, Response<LoginReponse> response) {
                AlertDialogManager.hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(1)) {
                        Toast.makeText(mActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        Log.e(TAG, "onResponse: "+response.body().getData().getFirst_name() );
                        Log.e(TAG, "onResponse: "+response.body().getData().getLast_name() );

                        hypnofyPreference.writeBoolean(mActivity, hypnofyPreference.IS_LOGIN, true);
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.USER_ID, response.body().getData().getId());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.EMAIL, response.body().getData().getEmail());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.FIRST_NAME, response.body().getData().getFirst_name());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.LAST_NAME, response.body().getData().getLast_name());
                        Intent intent = new Intent(mActivity, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else if (response.body().getStatus().equals(0)) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), response.body().getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<LoginReponse> call, Throwable t) {
                Log.e(TAG,"===ERROR===");
            }
        });
    }
}
