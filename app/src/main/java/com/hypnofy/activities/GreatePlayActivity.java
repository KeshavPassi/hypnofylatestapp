package com.hypnofy.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;
import com.hypnofy.fonts.CircularSeekBar;
import com.hypnofy.utils.Constants;
import com.hypnofy.utils.Utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.icu.text.DateFormat.getDateTimeInstance;
import static java.util.TimeZone.getTimeZone;


public class GreatePlayActivity extends BaseActivity {
    String TAG = GreatePlayActivity.this.getClass().getSimpleName();
    Activity mActivity = GreatePlayActivity.this;
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.imgPlayIV)
    ImageView imgPlayIV;
    @BindView(R.id.imgbackIV)
    ImageView imgbackIV;
    @BindView(R.id.txtDescriptionsTV)
    TextView txtDescriptionsTV;
    @BindView(R.id.welcomTExt)
    TextView welcomTExt;
    @BindView(R.id.circularSeekBar1)
    CircularSeekBar circularSeekBar1;
    @BindView(R.id.txtCurrent)
    TextView txtCurrent;
    @BindView(R.id.txtTotal)
    TextView txtTotal;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.BCKRLL)
    RelativeLayout BCKRLL;
    MediaPlayer mPlayer;
    boolean isPlayPause = true;
    public int mediaFileLengthInMilliseconds;
    boolean wasPlaying = false;

    private Handler mHandler = new Handler();
static String strTextSpeech,strTextGender,fileOutput;
    private TextToSpeech myTTS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greate_play);
        ButterKnife.bind(this);
        welcomTExt.setText("Great! Your personalised" + "\n" + "Hypno session is ready");
//        welcomTExt.setText("Please sit or lie down.");
        Bundle bundle = getIntent().getExtras();
        strTextSpeech = bundle.getString("message");
        strTextGender= Constants.strGender;
//        =============================================================
        mPlayer = new MediaPlayer();
//        mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.new_music);//Create MediaPlayer object with MP3 file under res/raw folder
// gets the files in the directory
        String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        //your path here!
        File fileDirectory = new File(exStoragePath + "/media/alarms");

// lists all the files into an array
        File[] dirFiles = fileDirectory.listFiles();

        if (dirFiles.length != 0) {
            // loops through the array of files, outputing the name to console
            for (int ii = 0; ii < dirFiles.length; ii++) {
                 fileOutput = dirFiles[ii].toString();
                System.out.println("FileNAme==="+fileOutput);
            }
        }

        Uri intentUri = Uri.fromFile(new File(strTextSpeech));
        Log.d("FileExist", "file " + intentUri.getPath());
        try {
            mPlayer .setDataSource(strTextSpeech);
            mPlayer.prepare();
//            mPlayer .setDataSource(fileOutput);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        long totalDuration = mPlayer.getDuration();
        long currentDuration = mPlayer.getCurrentPosition();
        txtTotal.setText("" + milliSecondsToTimer( mPlayer.getDuration()));
//        ===========================================================
//         Updating progress bar
        int progress = (int) (getProgressPercentage(currentDuration, totalDuration));
        circularSeekBar1.setProgress(progress);
//        ===========================================================
        seekbarChangeListner();

//myTTS = new TextToSpeech(mActivity,null);
//        myTTS  = new TextToSpeech(mActivity, new TextToSpeech.OnInitListener() {
//            @SuppressLint("NewApi")
//            @Override
//            public void onInit(int status) {
//                String voice = null;
////                Voice voiceobj = new Voice("en-us-x-sfg#male_3-local",
////                        Locale.getDefault(), 400, 200, false, null);
////                myTTS.setVoice(voiceobj);
//
//                Set<Voice> voices = myTTS.getVoices();
//                Log.e("TTSNAme1=== ", String.valueOf(voices));
//                for (Voice tmpVoice : myTTS.getVoices()) {
//                    Log.e("TTSNAme2=== ", String.valueOf(tmpVoice.getName()));
//                    if (tmpVoice.getName().contains("#male") && tmpVoice.getName().contains("en-uk")) {
//                        voice = String.valueOf(tmpVoice);
//                        myTTS.setVoice(tmpVoice);
//                        Log.e("TTSNAme=== ", String.valueOf(tmpVoice));
//                        break;
//                    }
//                    else {
//                        voice = null;
//                    }
//                }
//                if (voice != null) {
////                    myTTS.setVoice(voice);
//                }
//
////                myTTS.setLanguage(Locale.US);
////                myTTS.setPitch((float) 0.6);
//
//                myTTS.setSpeechRate(0.8f);
//                if (status == TextToSpeech.SUCCESS) {
//                    int result = myTTS.setLanguage(Locale.UK);
//
//                    if (result == TextToSpeech.LANG_MISSING_DATA
//                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
//                        Log.e("TTS", "This Language is not supported");
//                    } else {
//
////                        speakOut();
//                    }
//
//                } else {
//                    Log.e("TTS", "Initilization Failed!");
//                }
//            }
//        },"com.google.android.tts");

    }

    private void seekbarChangeListner() {
        circularSeekBar1.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
            @Override
            public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    int totalDuration = mPlayer.getDuration();
                    int currentPosition = Utilities.progressToTimer(circularSeekBar.getProgress(), totalDuration);
                    // forward or backward to certain seconds
                    mPlayer.seekTo(currentPosition);
                    // update timer progress again
                    updateProgressBar();
                }
                if (isPlayPause){
                    imgPlayIV.setImageResource(R.drawable.play_music);
                }else{
                    imgPlayIV.setImageResource(R.drawable.pause_icon);
                }
            }

            @Override
            public void onStopTrackingTouch(CircularSeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(CircularSeekBar seekBar) {

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick({R.id.imgCloseIV, R.id.imgPlayIV, R.id.imgbackIV, R.id.backRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCloseIV:

                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
                case R.id.backRL:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
            case R.id.imgbackIV:
                startActivity(new Intent(mActivity, PleaseTellUsActivity.class));
                finish();
                break;
                case R.id.BCKRLL:
                startActivity(new Intent(mActivity, PleaseTellUsActivity.class));
                finish();
                break;
            case R.id.imgPlayIV:
                mediaFileLengthInMilliseconds = mPlayer.getDuration(); // gets the song length in milliseconds from URL
                Log.v("MILISEC", String.valueOf(mediaFileLengthInMilliseconds));
                int insec = mediaFileLengthInMilliseconds / 1000;
                float length = insec / 60;
                txtTotal.setText(String.valueOf(length));
                if (!mPlayer.isPlaying()) {
                    play();
                } else {
                    pause();
                }

//                    String text =strTextSpeech;


                break;

        }
    }


//    public void pause(long duration){
//        myTTS.playSilence(duration, TextToSpeech.QUEUE_FLUSH, null);
//    }

    @Override
    protected void onPause() {
        super.onPause();
//        myTTS.stop();
//        myTTS.shutdown();
        if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.play_music);
            circularSeekBar1.setMax(mPlayer.getCurrentPosition());
            mPlayer.stop();//Stop playing the music
        }
        pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        myTTS.stop();
//        myTTS.shutdown();
        if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.play_music);
            circularSeekBar1.setMax(mPlayer.getCurrentPosition());
            mPlayer.stop();//Stop playing the music
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        /*if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.play_music);
            circularSeekBar1.setMax(mPlayer.getCurrentPosition());
            mPlayer.stop();//Stop playing the music
        }else {

        }*/
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        myTTS.stop();
//        myTTS.shutdown();
        if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.play_music);
            circularSeekBar1.setMax(mPlayer.getCurrentPosition());
            mPlayer.stop();//Stop playing the music
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        myTTS.stop();
//        myTTS.shutdown();
        startActivity(new Intent(mActivity, PleaseTellUsActivity.class));
        finish();
    }


    private void play() {
        try {
            isPlayPause = false;
            imgPlayIV.setImageResource(R.drawable.pause_icon);
            mPlayer.start();
            // Updating progress bar
            updateProgressBar();
            imgPlayIV.setImageDrawable(getResources().getDrawable(R.drawable.pause_icon));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pause() {
        try {
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.play_music);

            if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
                mPlayer.pause();
                updateProgressBar();
//                circularSeekBar1.setProgress(mPlayer.getCurrentPosition());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = mPlayer.getDuration();
            long currentDuration = mPlayer.getCurrentPosition();

//            if (totalDuration == currentDuration) {
//                imgPlayIV.setImageDrawable(getResources().getDrawable(R.drawable.play_music));
//            }
            txtTotal.setText("" + milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            txtCurrent.setText("" + milliSecondsToTimer(currentDuration));
            //int mCurrentPosition = mPlayer.getCurrentPosition() / 1000;
            //circularSeekBar1.setProgress(mCurrentPosition);

//            // Updating progress bar
            int progress = (int) (getProgressPercentage(currentDuration, totalDuration));
            circularSeekBar1.setProgress(progress);
            if (progress == 100){
                imgPlayIV.setImageResource(R.drawable.play_music);
            }

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    /**
     * Function to get Progress percentage
     *
     * @param currentDuration
     * @param totalDuration
     */
    public int getProgressPercentage(long currentDuration, long totalDuration) {
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage = (((double) currentSeconds) / totalSeconds) * 100;

        // return percentage
        return percentage.intValue();
    }

    /**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     */
    public String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }


    public void primarySeekBarProgressUpdater() {
        if (getApplicationContext() != null) {
            circularSeekBar1.setProgress(mPlayer.getCurrentPosition());
            int millies = mPlayer.getCurrentPosition();
            int seconds = millies / 1000;
            String sec = null;
            int minutes = seconds / 60;
            seconds = seconds % 60;
            sec = String.valueOf(seconds);
            if (sec.length() < 2) {
                sec = "0" + String.valueOf(seconds);
            }
            String duration = String.valueOf(minutes).concat(":".concat(sec));
            txtCurrent.setText(duration);

            if (mPlayer.isPlaying()) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        primarySeekBarProgressUpdater();
                    }
                };
                mHandler.postDelayed(runnable, 500);
//                Runnable notification = ()  {
//
//                    Log.v("NowD", String.valueOf(mPlayer.getCurrentPosition()));
//                };
//                handler.postDelayed(notification, 1000);
            }
        }
    }


}
