package com.hypnofy.activities;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.hypnofy.BaseActivity;
import com.hypnofy.R;
import com.hypnofy.beans.SignUpReponse;
import com.hypnofy.fragments.HomeFragment;
import com.hypnofy.fragments.ProfileFragment;
import com.hypnofy.retrofile.RestClient;
import com.hypnofy.utils.AlertDialogManager;
import com.hypnofy.utils.Constants;
import com.hypnofy.utils.Utilities;
import com.hypnofy.utils.hypnofyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {
    String TAG = HomeActivity.this.getClass().getSimpleName();
    Activity mActivity = HomeActivity.this;
    @BindView(R.id.containerLL)
    FrameLayout containerLL;
    @BindView(R.id.homeLL)
    LinearLayout homeLL;
    @BindView(R.id.profileLL)
    LinearLayout profileLL;
    @BindView(R.id.profileIMV)
    ImageView profileIMV;
    @BindView(R.id.homeIMV)
    ImageView homeIMV;
    @BindView(R.id.homeTV)
    TextView homeTV;
    @BindView(R.id.profileTV)
    TextView profileTV;


    /********
     *Replace Fragment In Activity
     **********/
    public void addFragmentToView(Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerLL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            ft.commitAllowingStateLoss();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
//        profileIMV.setImageDrawable(getResources().getDrawable(R.drawable.profile_black));
//        homeIMV.setImageDrawable(getResources().getDrawable(R.drawable.home_color));
//        homeTV.setTextColor(getResources().getColor(R.color.txt_color_home));
//        profileTV.setTextColor(getResources().getColor(R.color.black));
//        homeClick();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (Utilities.isNetworkAvailable(mActivity)) {
            executeAPI(hypnofyPreference.readString(mActivity, hypnofyPreference.USER_ID, ""));
        } else {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.internetconnection));
        }
    }

    private void executeAPI(String userID) {
        Constants.ProfilePicture="";
        Constants.Name="";
        AlertDialogManager.showProgressDialog(mActivity);
        RestClient.get().Profile(userID).enqueue(new Callback<SignUpReponse>() {
            @Override
            public void onResponse(Call<SignUpReponse> call, Response<SignUpReponse> response) {
                AlertDialogManager.hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(1)) {
//                        Toast.makeText(mActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        if (!response.body().getData().getFirst_name().equals("")) {
                            hypnofyPreference.writeString(mActivity, hypnofyPreference.FIRST_NAME, response.body().getData().getFirst_name());
                            Constants.Name =response.body().getData().getFirst_name() ;
                        }
                        if (!response.body().getData().getLast_name().equals("")) {
                            hypnofyPreference.writeString(mActivity, hypnofyPreference.LAST_NAME, response.body().getData().getLast_name());
                            Constants.Name =response.body().getData().getFirst_name() + " " + response.body().getData().getLast_name();
                        }
                        if(!response.body().getData().getUser_image().equals("")){
                            hypnofyPreference.writeString(mActivity, hypnofyPreference.USER_IMAGE, response.body().getData().getUser_image());
                            String strImage =response.body().getData().getUser_image();
                            Constants.ProfilePicture =response.body().getData().getUser_image() ;
                        }
                      if(Constants.IsEditProfile==true){
                            Constants.IsEditProfile=false;
                            profileIMV.setImageDrawable(getResources().getDrawable(R.drawable.profile_color));
                            homeIMV.setImageDrawable(getResources().getDrawable(R.drawable.home_black));
                            homeTV.setTextColor(getResources().getColor(R.color.black));
                            profileTV.setTextColor(getResources().getColor(R.color.txt_color_home));
                            profileClick();
                        }else {
                            profileIMV.setImageDrawable(getResources().getDrawable(R.drawable.profile_black));
                            homeIMV.setImageDrawable(getResources().getDrawable(R.drawable.home_color));
                            homeTV.setTextColor(getResources().getColor(R.color.txt_color_home));
                            profileTV.setTextColor(getResources().getColor(R.color.black));
                            homeClick();
                        }
                    } else if (response.body().getStatus().equals(0)) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), response.body().getMessage());
                    }
                }
            }
            @Override
            public void onFailure(Call<SignUpReponse> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e("", "====ERROR===");
            }
        });
    }

    @OnClick({R.id.homeLL, R.id.profileLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.homeLL:
                profileIMV.setImageDrawable(getResources().getDrawable(R.drawable.profile_black));
                homeIMV.setImageDrawable(getResources().getDrawable(R.drawable.home_color));
                homeTV.setTextColor(getResources().getColor(R.color.txt_color_home));
                profileTV.setTextColor(getResources().getColor(R.color.black));
                homeClick();
                break;
            case R.id.profileLL:
                profileIMV.setImageDrawable(getResources().getDrawable(R.drawable.profile_color));
                homeIMV.setImageDrawable(getResources().getDrawable(R.drawable.home_black));
                homeTV.setTextColor(getResources().getColor(R.color.black));
                profileTV.setTextColor(getResources().getColor(R.color.txt_color_home));
                profileClick();
                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void homeClick() {
        addFragmentToView(new HomeFragment(), false, null);
    }

    private void profileClick() {
        addFragmentToView(new ProfileFragment(), false, null);
    }
}
