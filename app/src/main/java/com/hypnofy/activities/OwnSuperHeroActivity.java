package com.hypnofy.activities;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OwnSuperHeroActivity extends BaseActivity {
    String TAG = OwnSuperHeroActivity.this.getClass().getSimpleName();
    Activity mActivity = OwnSuperHeroActivity.this;
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.imgPlayIV)
    ImageView imgPlayIV;
    @BindView(R.id.btnNextGO)
    Button btnNextGO;
    MediaPlayer mPlayer;
    boolean isPlayPause = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_own_super_hero);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.imgCloseIV, R.id.imgPlayIV, R.id.btnNextGO})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCloseIV:
                onBackPressed();
                break;
            case R.id.imgPlayIV:
                if (isPlayPause) {
                    play();
                } else {
                    pause();
                }
                break;
            case R.id.btnNextGO:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.ic_play_circle_24dp);
            mPlayer.stop();//Stop playing the music
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.ic_play_circle_24dp);
            mPlayer.stop();//Stop playing the music
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.ic_play_circle_24dp);
            mPlayer.stop();//Stop playing the music
        }
    }


    private void play() {
        try {
            isPlayPause = false;
            imgPlayIV.setImageResource(R.drawable.ic_pause_24dp);
            //Toast.makeText(mActivity, "Play", Toast.LENGTH_SHORT).show();
            mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.new_music);//Create MediaPlayer object with MP3 file under res/raw folder
            mPlayer.start();//Start playing the music
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void pause() {
        try {
            isPlayPause = true;
            imgPlayIV.setImageResource(R.drawable.ic_play_circle_24dp);
            //Toast.makeText(mActivity, "Pause", Toast.LENGTH_SHORT).show();
            if (mPlayer != null && mPlayer.isPlaying()) {//If music is playing already
                mPlayer.stop();//Stop playing the music
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
