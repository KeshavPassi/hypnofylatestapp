package com.hypnofy.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hypnofy.R;
import com.hypnofy.beans.ForgotReponse;
import com.hypnofy.beans.Login;
import com.hypnofy.beans.LoginReponse;
import com.hypnofy.retrofile.RestClient;
import com.hypnofy.utils.AlertDialogManager;
import com.hypnofy.utils.Utilities;
import com.hypnofy.utils.hypnofyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPwdActivity extends AppCompatActivity {
    String TAG = ForgotPwdActivity.this.getClass().getSimpleName();
    Activity mActivity = ForgotPwdActivity.this;
    @BindView(R.id.backLL)
    LinearLayout backLL;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pwd);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.backLL, R.id.btnLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backLL:
                onBackPressed();
                break;
            case R.id.btnLogin:
                validate();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void validate() {
        if (editEmailET.getText().toString().trim().equals("")) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_email));
        } else if (!Utilities.isValidEmaillId(editEmailET.getText().toString())) {
            AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.please_enter_valid_email));
        } else {
            //Execute Api
            if (Utilities.isNetworkAvailable(mActivity)) {
//                finish();
                executeAPI(editEmailET.getText().toString());
//                Toast.makeText(mActivity, "Check Your Email!", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), getString(R.string.internetconnection));
            }
        }
    }

    private void executeAPI(String email) {
        AlertDialogManager.showProgressDialog(mActivity);
//        http://dharmani.com/test_hp/Forgotpassword.php
        RestClient.get().forgotpassword(email).enqueue(new Callback<ForgotReponse>() {
            @Override
            public void onResponse(Call<ForgotReponse> call, Response<ForgotReponse> response) {
                AlertDialogManager.hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(1)) {
                        Toast.makeText(mActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();

                        Log.e(TAG, "onResponse: " + response.body());
                        showAlertDialog(getString(R.string.app_name),response.body().getMessage());

                    } else if (response.body().getStatus().equals(0)) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), response.body().getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotReponse> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "===ERROR===");
            }
        });
    }

    public  void showAlertDialog( String strTitle, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtTitleTV = (TextView) alertDialog.findViewById(R.id.txtTitleTV);
        TextView txtMessageTV = (TextView) alertDialog.findViewById(R.id.txtMessageTV);
        Button btnDismiss = (Button) alertDialog.findViewById(R.id.btnDismiss);

        txtTitleTV.setText(strTitle);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                Intent intent = new Intent(mActivity, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        alertDialog.show();
    }

}
