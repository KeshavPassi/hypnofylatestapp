package com.hypnofy.activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;
import com.hypnofy.utils.AlertDialogManager;
import com.hypnofy.utils.Constants;
import com.hypnofy.utils.hypnofyPreference;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PleaseTellUsActivity extends BaseActivity {
    Activity mActivity = PleaseTellUsActivity.this;
    String TAG = PleaseTellUsActivity.this.getClass().getSimpleName();
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.imgbackIV)
    ImageView imgbackIV;
    public static final int REQUEST_PERMISSION_CODE = 100;
    @BindView(R.id.welcomTExt)
    TextView welcomTExt;
    @BindView(R.id.btnSave)
    TextView btnSave;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editEmail2ET)
    EditText editEmail2ET;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    String speakTextTxt;
    TextToSpeech myTTS;
    HashMap myHashRender = new HashMap();
    String tempDestFile;

    String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
    //your path here!
    File appTmpPath = new File(exStoragePath + "/media/alarms");
    private String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    private String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    private String writeCamera = Manifest.permission.CAMERA;
    private ProgressDialog mProgressDialog;

    //String tempFilename = "tmpaudio.wav";
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_please_tell_us);
        ButterKnife.bind(this);
        mProgressDialog = new ProgressDialog(this);
//        Bundle bundle = getIntent().getExtras();
// // Close the dialog window on pressing back button
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);

        // Setting a horizontal style progress bar
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        /** Setting a message for this progress dialog
         * Use the method setTitle(), for setting a title
         * for the dialog window
         *  */
        mProgressDialog.setMessage("Please wait ...");

//        strTextGender = bundle.getString("male");
        myTTS = new TextToSpeech(mActivity, null);


        welcomTExt.setText("Please tell us more");
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick({R.id.imgCloseIV, R.id.txtOneLL, R.id.imgbackIV, R.id.txttwoLL, R.id.txtthreeLL, R.id.btnSave, R.id.backRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCloseIV:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
            case R.id.backRL:
                startActivity(new Intent(mActivity, HomeActivity.class));
                finish();
                break;
            case R.id.imgbackIV:
                Intent mIntent5 = new Intent(mActivity, MaleAndFemaleActivity.class);
                startActivity(mIntent5);
                finish();
                break;
            case R.id.BCKRL:
                Intent mIntent25 = new Intent(mActivity, MaleAndFemaleActivity.class);
                startActivity(mIntent25);
                finish();
                break;
            case R.id.btnSave:
                // Default text to play in Nexr screen
//                String text = "Hello   " + hypnofyPreference.readString(mActivity,hypnofyPreference.FIRST_NAME,"")+"          "+"   " + "                " +       getResources().getString(R.string.text_play).toString();
                if (editEmailET.getText().toString().equals("")) {
                    AlertDialogManager.showAlertDialog(mActivity, getResources().getString(R.string.app_name), "Please enter first field");
                } else {
                    setUpCameraGalleryDialog();
                }
                break;
//            case R.id.txtBeachInduction:
//                Intent mIntent1 = new Intent(mActivity,GreatePlayActivity.class);
//                mIntent1.putExtra("TYPE", txtBeachInduction.getText().toString().trim());
//                startActivity(mIntent1);
//                finish();
//                break;
//            case R.id.txtFullBodyRelaxTV:
//                Intent mIntent2 = new Intent(mActivity,GreatePlayActivity.class);
//                mIntent2.putExtra("TYPE", txtFullBodyRelaxTV.getText().toString().trim());
//                startActivity(mIntent2);
//                finish();
//                break;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void setUpCameraGalleryDialog() {
        if (checkPermission()) {
            mProgressDialog.show();
            String text = editEmailET.getText().toString();

            //                  Voice voiceobj = new Voice("en-us-x-sfg#male_3-local",
            final Voice voiceobj = new Voice("en-gb-x-fis#male_3-local",
//                    Voice voiceobj = new Voice("en-gb-x-fis#male_3-local", >>
//                    Voice voiceobj = new Voice("en-gb-x-rjs#male_3-local",

                    Locale.getDefault(), 400, 200, false, null);


            String editText1 = text;
            speakTextTxt = "Hello world66";
            HashMap myHashRender = new HashMap();
            myHashRender.put(TextToSpeech.Engine.KEY_FEATURE_EMBEDDED_SYNTHESIS, myTTS.setVoice(voiceobj));
            myHashRender.put(myTTS, editText1);
//                myHashRender.put(TextToSpeech.Engine.EXTRA_AVAILABLE_VOICES,"male");


            Log.d("MainActivity", "exStoragePath : " + exStoragePath);

            boolean isDirectoryCreated = appTmpPath.mkdirs();
            Log.d("MainActivity", "directory " + appTmpPath + " is created : " + isDirectoryCreated);

            tempDestFile = appTmpPath.getAbsolutePath() + File.separator + speakTextTxt + ".mp3";
            Log.d("MainActivity", "tempDestFile : " + tempDestFile);
            Constants.DestinationFile = tempDestFile;
            new MySpeech(editText1);
        } else {
            requestPermission();
        }


    }

    // test
    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, REQUEST_PERMISSION_CODE);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    setUpCameraGalleryDialog();

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    requestPermission();
                }
                return;
            }

        }
    }

    class MySpeech implements TextToSpeech.OnInitListener {

        String tts;
        Context comtact;

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        public MySpeech(String tts) {
            this.tts = tts;

            myTTS = new TextToSpeech(mActivity, this);
            //                  Voice voiceobj = new Voice("en-us-x-sfg#male_3-local",

            myTTS.setSpeechRate(0.8f);

        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onInit(int status) {
            int abc = 0;
            int SPLASH_TIME_OUT = 3000;
//                String text = "Hello   " + hypnofyPreference.readString(mActivity,hypnofyPreference.FIRST_NAME,"")+"          "+"   " + "                " +       getResources().getString(R.string.text_play).toString();
            String text = editEmailET.getText().toString();

            if (Constants.strGender.equals("male")) {
                //                  Voice voiceobj = new Voice("en-us-x-sfg#male_3-local",
                Voice voiceobj = new Voice("en-in-x-ahp#male_3-local",
//                    Voice voiceobj = new Voice("en-gb-x-fis#male_3-local", >>
//                    Voice voiceobj = new Voice("en-gb-x-rjs#male_3-local",
                        Locale.getDefault(), 400, 200, false, null);
                myTTS.setVoice(voiceobj);
                myTTS.setSpeechRate(0.8f);
                final String editText1 = text;
                Log.v("log", "initi");
                //                    //textToSpeech can only cope with Strings with < 4000 characters
                int dividerLimit = 3900;

                if (text.length() >= dividerLimit) {
                    int textLength = text.length();
                    ArrayList<String> texts = new ArrayList<String>();
                    int count = textLength / dividerLimit + ((textLength % dividerLimit == 0) ? 0 : 1);
                    int start = 0;
                    int end = text.indexOf(" ", dividerLimit);
                    for (int i = 1; i <= count; i++) {
                        texts.add(text.substring(start, end));
                        start = end;
                        if ((start + dividerLimit) < textLength) {
                            end = text.indexOf(" ", start + dividerLimit);
                        } else {
                            end = textLength;
                        }
                    }
                    for (int i = 0; i < texts.size(); i++) {
//                            textToSpeech.speak(texts.get(i), TextToSpeech.QUEUE_ADD, null);
                        abc = myTTS.synthesizeToFile(texts.get(i), myHashRender, tempDestFile);
                    }
                } else {
//                        myTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                    abc = myTTS.synthesizeToFile(text, myHashRender, tempDestFile);
                }

                if (abc == TextToSpeech.SUCCESS) {
//
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialogManager.showProgressDialog(mActivity);
                            Toast toast = Toast.makeText(mActivity, "Saved " + editText1,
                                    Toast.LENGTH_SHORT);
                            toast.show();
                            Intent intent = new Intent(mActivity, GreatePlayActivity.class);
                            intent.putExtra("message", tempDestFile);

                            startActivity(intent);
                            finish();
                            mProgressDialog.dismiss();
                        }
                    }, SPLASH_TIME_OUT);
//                    Intent intent = new Intent(mActivity, GreatePlayActivity.class);
//                    intent.putExtra("message", editText1);
//
//                    startActivity(intent);

                }
            } else if (Constants.strGender.equals("female")) {
                Voice voiceobj = new Voice("en-uk-x-ahp#female_2-local",
//                Voice voiceobj = new Voice("en-gb-x-fis#male_3-local",
                        Locale.getDefault(), 400, 200, false, null);
                myTTS.setVoice(voiceobj);
                myTTS.setSpeechRate(0.8f);
                final String editText1 = text;
                Log.v("log", "initi");
                //                    //textToSpeech can only cope with Strings with < 4000 characters
                int dividerLimit = 3900;

                if (text.length() >= dividerLimit) {
                    int textLength = text.length();
                    ArrayList<String> texts = new ArrayList<String>();
                    int count = textLength / dividerLimit + ((textLength % dividerLimit == 0) ? 0 : 1);
                    int start = 0;
                    int end = text.indexOf(" ", dividerLimit);
                    for (int i = 1; i <= count; i++) {
                        texts.add(text.substring(start, end));
                        start = end;
                        if ((start + dividerLimit) < textLength) {
                            end = text.indexOf(" ", start + dividerLimit);
                        } else {
                            end = textLength;
                        }
                    }
                    for (int i = 0; i < texts.size(); i++) {
//                            textToSpeech.speak(texts.get(i), TextToSpeech.QUEUE_ADD, null);
                        abc = myTTS.synthesizeToFile(texts.get(i), myHashRender, tempDestFile);
                    }
                } else {
                    abc = myTTS.synthesizeToFile(text, myHashRender, tempDestFile);
//                        myTTS.speak(text, TextToSpeech.QUEUE_FLUSH, null);
                }

                if (abc == TextToSpeech.SUCCESS) {
//                        Toast toast = Toast.makeText(mActivity, "Saved " + editText1,
//                                Toast.LENGTH_SHORT);
//                        toast.show();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                                AlertDialogManager.showProgressDialog(mActivity);
                            Toast toast = Toast.makeText(mActivity, "Saved " + editText1,
                                    Toast.LENGTH_SHORT);
                            toast.show();
                            Intent intent = new Intent(mActivity, GreatePlayActivity.class);
                            intent.putExtra("message", tempDestFile);

                            startActivity(intent);
                            finish();
                            mProgressDialog.dismiss();
                        }
                    }, SPLASH_TIME_OUT);


                }
            }

            System.out.println("Result : " + abc);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent mIntent5 = new Intent(mActivity, MaleAndFemaleActivity.class);
        startActivity(mIntent5);
        finish();
    }
}
