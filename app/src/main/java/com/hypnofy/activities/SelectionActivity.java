package com.hypnofy.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.hypnofy.BaseActivity;
import com.hypnofy.R;
import com.hypnofy.beans.EditProfileResponse;
import com.hypnofy.beans.FaceBookResponce;
import com.hypnofy.beans.SignUpReponse;
import com.hypnofy.retrofile.RestClient;
import com.hypnofy.utils.AlertDialogManager;
import com.hypnofy.utils.hypnofyPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectionActivity extends BaseActivity {
    Activity mActivity = SelectionActivity.this;
    String TAG = SelectionActivity.this.getClass().getSimpleName();


    @BindView(R.id.txtAlreadyMemberTV)
    TextView txtAlreadyMemberTV;
    @BindView(R.id.signLL)
    LinearLayout signLL;
    @BindView(R.id.facebookLL)
    LinearLayout facebookLL;
    String fbUserID,fbToken,str_facebookid,str_facebookname,str_facebooklastname,imageUrl;
    private LoginButton login_button;
    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        ButterKnife.bind(this);

    }

    @OnClick({R.id.signLL, R.id.facebookLL, R.id.txtAlreadyMemberTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.signLL:
                startActivity(new Intent(mActivity, SignUpActivity.class));
                finish();
                break;
            case R.id.facebookLL:
                LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
                loginWithFacebook();
                break;
            case R.id.txtAlreadyMemberTV:
                startActivity(new Intent(mActivity, LoginActivity.class));
                finish();
                break;
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    private void loginWithFacebook() {

        login_button = (LoginButton) findViewById(R.id.login_button);
//        login_button_facebook.setReadPermissions("user_friends");
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                getFbProfile(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                Toast.makeText(mActivity, "Cancelled by User", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Toast.makeText(mActivity, exception.toString(), Toast.LENGTH_SHORT).show();
                Log.i("", "");
            }
        });
    }

    private void getFbProfile(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                String strAccessToken = accessToken.getToken();
                Log.e(TAG, "*****Token*****" + strAccessToken);
                fbUserID = accessToken.getUserId();
                fbToken = strAccessToken;

                try {
                    str_facebookid = object.getString("id");
                    str_facebookname = object.getString("first_name");
                    str_facebooklastname = object.getString("last_name");
                    imageUrl = "https://graph.facebook.com/" + fbUserID + "/picture?type=large";

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                executeLoginWithFacebook(str_facebookname,str_facebooklastname,str_facebookname+str_facebooklastname+"@gmail.com","1",imageUrl,str_facebookid);
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link, email,last_name,first_name");
        request.setParameters(parameters);
        request.executeAsync();
    }
    private void executeLoginWithFacebook(String fname, String lname, String email, String password,String strfacebookImage,String strCheckstatus) {
        AlertDialogManager.showProgressDialog(mActivity);
        Map<String, String> data = new HashMap<>();
        data.put("first_name", fname);
        data.put("last_name", lname);
        data.put("email", email);
        data.put("terms", "1");
        data.put("user_image", strfacebookImage);
        data.put("facebook_id", strCheckstatus);
        RestClient.get().facebookSignUp(data).enqueue(new Callback<FaceBookResponce>() {
            @Override
            public void onResponse(Call<FaceBookResponce> call, Response<FaceBookResponce> response) {
                AlertDialogManager.hideProgressDialog();
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equals(1)) {
                        hypnofyPreference.writeBoolean(mActivity, hypnofyPreference.IS_LOGIN, true);
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.USER_ID, response.body().getData().getId());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.EMAIL, response.body().getData().getEmail());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.FIRST_NAME, response.body().getData().getFirst_name());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.LAST_NAME, response.body().getData().getLast_name());
                        hypnofyPreference.writeString(mActivity, hypnofyPreference.USER_IMAGE, response.body().getData().getUser_image());
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                    else if (response.body().getStatus().equals(0)) {
                        AlertDialogManager.showAlertDialog(mActivity, getString(R.string.error), response.body().getMessage());
                    }
                }else {

                }
            }

            @Override
            public void onFailure(Call<FaceBookResponce> call, Throwable t) {
                AlertDialogManager.hideProgressDialog();
                Log.e(TAG, "====ERROR===");
            }
        });
    }
    }

