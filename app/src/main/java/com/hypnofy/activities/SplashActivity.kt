package com.hypnofy.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import com.hypnofy.BaseActivity
import com.hypnofy.R

class SplashActivity : BaseActivity() {
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 1500 //1.5 seconds

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setSplash();

    }

    fun setSplash() {
        //Initialize the Handler
        mDelayHandler = Handler()
        //Navigate with delay
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
    }


    val mRunnable: Runnable = Runnable {
        val intent = Intent(applicationContext, SelectionActivity::class.java)
        startActivity(intent)
        finish()
    }
}
