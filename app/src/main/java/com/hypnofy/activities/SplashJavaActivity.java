package com.hypnofy.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.hypnofy.BaseActivity;
import com.hypnofy.R;
import com.hypnofy.utils.hypnofyPreference;

/**
 * Created by android-da on 9/21/18.
 */

public class SplashJavaActivity extends BaseActivity {
    Activity mActivity = SplashJavaActivity.this;
    String TAG = SplashJavaActivity.this.getClass().getSimpleName();
    int SPLASH_TIME_OUT = 1500;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setSplash();
    }


    private void setSplash() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (hypnofyPreference.readBoolean(mActivity, hypnofyPreference.IS_LOGIN, false)) {
                    startActivity(new Intent(mActivity, HomeActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(mActivity, SelectionActivity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

}
