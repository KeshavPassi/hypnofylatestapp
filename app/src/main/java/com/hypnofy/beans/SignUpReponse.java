package com.hypnofy.beans;

import java.io.Serializable;

/**
 * Created by android-da on 9/21/18.
 */

public class SignUpReponse implements Serializable {
    private Integer status;
    private String message;
    private SignUp data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public SignUp getData() {
        return data;
    }

    public void setData(SignUp data) {
        this.data = data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
