package com.hypnofy.beans;

import java.io.Serializable;

/**
 * Created by android-da on 9/21/18.
 */

public class LoginReponse implements Serializable {
    private Integer status;
    private Login data;
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Login getData() {
        return data;
    }

    public void setData(Login data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

//   "status": 1,
//           "message": "Password sent on this email"