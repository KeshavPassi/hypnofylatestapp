package com.hypnofy.beans;

import java.io.Serializable;

/**
 * Created by dharmaniz on 27/9/18.
 */

public class EditProfileResponse implements Serializable {
    private Integer status;

    private String message="";

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
