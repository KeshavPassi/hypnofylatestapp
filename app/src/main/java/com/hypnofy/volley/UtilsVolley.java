package com.hypnofy.volley;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.widget.ProgressBar;


import com.hypnofy.R;

import org.json.JSONException;
import org.json.JSONObject;

import static android.view.Window.FEATURE_NO_TITLE;

public class UtilsVolley {
    static Dialog pdialog;

    public static String trimMessage(String json, String key) {
        String trimmedString = null;

        try {
            JSONObject obj = new JSONObject(json);
            trimmedString = obj.getString(key);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return trimmedString;
    }
    public static void showProgressDialog(Activity mActivity) {
        pdialog = new Dialog(mActivity);
        pdialog.requestWindowFeature(FEATURE_NO_TITLE);
        pdialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setCancelable(false);
        pdialog.setContentView(R.layout.dialog_progress);

        ProgressBar circlePB = (ProgressBar) pdialog.findViewById(R.id.circlePB);
//        circlePB.getIndeterminateDrawable().setColorFilter(0xFF313131,
//                android.graphics.PorterDuff.Mode.MULTIPLY);

        pdialog.show();
    }
    public static void hideProgressDialog() {
        if (pdialog.isShowing()) {
            pdialog.dismiss();
        }
    }
}