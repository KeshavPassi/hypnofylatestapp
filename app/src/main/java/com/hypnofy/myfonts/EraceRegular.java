package com.hypnofy.myfonts;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by dharmaniz on 25/9/18.
 */

public class EraceRegular {
    public static Typeface fontTypeface ;
    String path = "ERASLGHT.TTF";
    Context mContext;

    public EraceRegular() {
    }
    public EraceRegular(Context context) {
        mContext = context;
    }

    public Typeface getFont(){
        if(fontTypeface==null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        return fontTypeface;
    }
}
