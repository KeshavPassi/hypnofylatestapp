package com.hypnofy.myfonts;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by dharmaniz on 25/9/18.
 */

public class EraceMedium {
    public static Typeface fontTypeface ;
    String path = "ERASMD.TTF";
    Context mContext;

    public EraceMedium() {
    }
    public EraceMedium(Context context) {
        mContext = context;
    }

    public Typeface getFont(){
        if(fontTypeface==null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        return fontTypeface;
    }

}
