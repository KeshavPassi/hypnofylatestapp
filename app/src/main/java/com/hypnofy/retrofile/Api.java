package com.hypnofy.retrofile;


import com.hypnofy.beans.EditProfileResponse;
import com.hypnofy.beans.FaceBookResponce;
import com.hypnofy.beans.ForgotReponse;
import com.hypnofy.beans.Login;
import com.hypnofy.beans.LoginReponse;
import com.hypnofy.beans.SignUpReponse;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Dharmaniz-raman on 19/9/2018.
 */

public interface Api {

    @POST("Login.php")
    Call<LoginReponse> login(@Query("email") String email,
                          @Query("password") String password);

//    @FormUrlEncoded
// @POST("EditProfile.php")
//    Call<EditProfileResponse> editProfile(@Query("user_id") String userID,
//                                    @Field("user_image") String userImage,
//                                          @Query("last_name") String lastname,
//                                          @Query("first_name") String firstname);
@FormUrlEncoded
 @POST("EditProfile.php")
    Call<EditProfileResponse> editProfile( @FieldMap Map<String, String> options);

//@Multipart
//@POST("EditProfile.php")
//Call<EditProfileResponse> editProfile ( @Part("user_image ") RequestBody file , @Part("user_id") RequestBody id, @Part("last_name") RequestBody last_name, @Part("first_name") RequestBody first_name);

    @POST("Forgotpassword.php")
    Call<ForgotReponse> forgotpassword(@Query("email") String email);


    @POST("SignUp.php")
    Call<SignUpReponse> signup(@Query("first_name") String firstname,
                               @Query("last_name") String lastname,
                               @Query("email") String email,
                               @Query("password") String password,
                               @Query("terms") String terms);
//    http://dharmani.com/test_hp/Signupfacebook.php
//    @POST("Signupfacebook.php")
//    Call<FaceBookResponce> facebookSignUp(@Query("first_name") String firstname,
//                                  @Query("last_name") String lastname,
//                                  @Query("email") String email,
//                                  @Query("terms") String terms,
//                                          @Query("user_image") String user_image,
//                                  @Query("facebook_id") String facenook_id);
    @FormUrlEncoded
    @POST("Signupfacebook.php")
    Call<FaceBookResponce> facebookSignUp( @FieldMap Map<String, String> options);

    @GET("Profile.php")
    Call<SignUpReponse> Profile(@Query("user_id") String user_id);


//    @GET("fine/description")
//    Call<AddFine> fineUpdate(@Query("id") int id);
//
//    @HTTP(method = "DELETE", path = "team/cancel-membership", hasBody = true)
//    Call<PojoDefault> joinNewTeam();
//
//    @GET("notification")
//    Call<List<Notifications>> notifications();
//
//    @FormUrlEncoded
//    @HTTP(method = "DELETE", path = "team/remove-member", hasBody = true)
//    Call<PojoDefault> deleteMember(@Field("member_id") int member_id);
//
//    @GET("currency")
//    Call<MasterTeam> savingCurrency(@Query("currency_code") String currency_code, @Query("symbol") String symbol);
//
//    @FormUrlEncoded
//    @HTTP(method = "DELETE", path = "fine/delete", hasBody = true)
//    Call<PojoDefault> deleteFine(@Field("member_id") int member_id, @Field("fine_id") int fine_id);
//
//    @GET("https://v3.exchangerate-api.com/bulk/d8b36c5d46be769bb9328cb2/USD")
//    Call<Target> currencyConvertor();
//
//    @POST("fine/dues")
//    Call<PojoDefault> fineDues();
//
//    @FormUrlEncoded
//    @POST("team-member/payment")
//    Call<PojoDefault> memberFinePayment(@Field("amount") Float amount, @Field("member_id") Integer member_id);
//
//
//    @FormUrlEncoded
//    @POST("send/message")
//    Call<Messages> sendMessage(@Field("message_type") Integer message_type, @Field("messages") String messages, @Field("image") String image, @Field("team_id") int team_id, @Field("tags") String tags);
//
//    @GET("notification/unread")
//    Call<Notifications> notificationsCount();
//
//    @FormUrlEncoded
//    @POST("fine/pay")
//    Call<FinePay> finePay(@Field("is_paid") int is_paid, @Field("fine_id") int fine_id);
//
//
//    @FormUrlEncoded
//    @POST("team/join")
//    Call<ImageUpload> joinTeam(@Field("passcode") String passcode, @Field("team_name") String team_name, @Field("master_last_name") String master_last_name);
//
//    @GET("team/members-list")
//    Call<SquadMemberScreen> squadMembers();
//
//    @GET("get-messages")
//    Call<List<Messages>> getMessages(@Query("last_chat_id") int last_chat_id);
//
//    @GET("view/recent-message")
//    Call<List<Messages>> recentMessages(@Query("message_id") int message_id);
//
//    @GET("view/previous-message")
//    Call<List<Messages>> previousMessages(@Query("message_id") int message_id);
//
//    @FormUrlEncoded
//    @POST("team/join-request")
//    Call<PojoDefault> joinRequests(@Field("is_joined") int is_joined,
//                                   @Field("member_id") int member_id);
//
//    @FormUrlEncoded
//    @POST("team/new-master")
//    Call<ChangeMaster> newMaster(@Field("member_id") int member_id);
//
//    @FormUrlEncoded
//    @POST("fine/double")
//    Call<PojoDefault> doubleFineFeature(@Field("is_double") int is_double);
//
//    @POST("fine-type/add")
//    Call<PojoDefault> addFineOnPlayers(@Body Users users);
//
//    @GET("suggested-fines/list")
//    Call<List<Fine>> suggestedFineList();
//
//    @POST("user/logout")
//    Call<Profile> logOut();
//
//    @GET("fine-type/list")
//    Call<List<Fine>> fineList();
//
//    @GET("fine/history")
//    Call<List<Fine>> fineHistory(@Query("member_id") int member_id);
//
//    @GET("profile")
//    Call<Profile> playerProfile();
//
//    @GET("profile")
//    Call<Profile> otherPlayerProfile(@Query("id") int id);
//
//    @GET("target")
//    Call<Target> targetRecord();
//
//    @GET("fine/status")
//    Call<FineStatus> newFineStatus();
//
//    @FormUrlEncoded
//    @POST("password/forgot")
//    Call<Profile> forgotpassword(@Field("email") String email);
//
//    @FormUrlEncoded
//    @HTTP(method = "DELETE", path = "fine-type/remove", hasBody = true)
//    Call<PojoDefault> removeFine(@Field("fine_type_id") int fine_type_id);
//
//    @FormUrlEncoded
//    @POST("user/signin")
//    Call<PojoLogin> login(@Field("device_type") int device_type,
//                          @Field("device_id") String device_id,
//                          @Field("device_token") String device_token,
//                          @Field("app_version") int app_version,
//                          @Field("email") String email,
//                          @Field("password") String password);
//
//
//    @FormUrlEncoded
//    @POST("fine-type/save")
//    Call<AddFine> addFineSave(@Field("amount") float amount, @Field("name") String name,
//                              @Field("description") String description);
//
//    @FormUrlEncoded
//    @POST("fine-type/save")
//    Call<AddFine> addFineSave2(@Field("id") int id, @Field("amount") int amount, @Field("name") String name,
//                               @Field("description") String description);
//
//    @FormUrlEncoded
//    @PUT("target-amount")
//    Call<PojoDefault> setTarget(@Field("target_amount") float target_amount);
//
//    @PUT("profile")
//    Call<PojoDefault> putData(@Body Profile profile);
//
//    @FormUrlEncoded
//    @PUT("profile")
//    Call<PojoDefault> putSubscriptionDate(@Field("subscribe_until") String subscribe_until, @Field("subscribe_type") Integer subscribe_type);
//
//    @FormUrlEncoded
//    @POST("password/change")
//    Call<Profile> passwordChange(@Field("old_password") String old_password, @Field("new_password") String new_password);
//
//    @Multipart
//    @POST("upload")
//    Call<ImageUpload> postImage(@Part MultipartBody.Part file);
//
//    @FormUrlEncoded
//    @POST("team/create")
//    Call<Team> createTeam(@Field("team_name") String team_name, @Field("passcode") String passcode, @Field("logo") String logo);
//
//    @FormUrlEncoded
//    @POST("team/create")
//    Call<Team> EditTeam(@Field("id") String id, @Field("team_name") String team_name, @Field("passcode") String passcode, @Field("logo") String logo);
//
//    @FormUrlEncoded
//    @POST("user/signup")
//    Call<PojoLogin> signUp(@Field("device_type") int device_type,
//                           @Field("device_id") String device_id,
//                           @Field("device_token") String device_token,
//                           @Field("app_version") int app_version,
//                           @Field("email") String email,
//                           @Field("password") String password,
//                           @Field("first_name") String first_name,
//                           @Field("last_name") String last_name,
//                           @Field("profile_image") String profile_image,
//                           @Field("is_master") int is_master);


}


