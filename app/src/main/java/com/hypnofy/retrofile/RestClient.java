package com.hypnofy.retrofile;


import com.hypnofy.utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Dharmaniz-raman on 19/9/2018.
 */

public class RestClient {
    private static Api REST_CLIENT;
    private static Retrofit retrofit;


    static {
        setupRestClient();
    }

    private RestClient() {
    }

    public static Api get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.SERVER_URL)
                .client(getOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        REST_CLIENT = retrofit.create(Api.class);

    }


    private static OkHttpClient getOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(5000, TimeUnit.SECONDS);
        httpClient.readTimeout(5000, TimeUnit.SECONDS);
        // add your other interceptors …
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Content-Type","application/json").build();
                return chain.proceed(request);
            }
        });
        // add logging as last interceptor
        httpClient.addNetworkInterceptor(logging);

        return httpClient.build();
    }

//    public static Retrofit getRetrofitInstance() {
//        return retrofit;
//    }
}
