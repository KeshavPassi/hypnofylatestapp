package com.hypnofy.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.hypnofy.R;
import com.hypnofy.activities.EditProfileActivity;
import com.hypnofy.activities.HomeActivity;
import com.hypnofy.activities.LoginActivity;
import com.hypnofy.beans.SignUpReponse;
import com.hypnofy.retrofile.RestClient;
import com.hypnofy.utils.AlertDialogManager;
import com.hypnofy.utils.Constants;
import com.hypnofy.utils.Utilities;
import com.hypnofy.utils.hypnofyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    View mView;
    @BindView(R.id.ImageViewIV)
    CircleImageView ImageViewIV;
    @BindView(R.id.nameTv)
    TextView nameTv;
    @BindView(R.id.editTv)
    TextView editTv;
    @BindView(R.id.logoutLL)
    LinearLayout logoutLL;
    @BindView(R.id.replayLL)
    LinearLayout replayLL;
    @BindView(R.id.subscriberLL)
    LinearLayout subscriberLL;
    @BindView(R.id.paymentLL)
    LinearLayout paymentLL;
    @BindView(R.id.sessionLL)
    LinearLayout sessionLL;
    @BindView(R.id.preferLL)
    LinearLayout preferLL;
    @BindView(R.id.reminderLL)
    LinearLayout reminderLL;
@BindView(R.id.progressLL)
    ProgressBar progressLL;
    Unbinder unbinder;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_profile, container, false);


        unbinder = ButterKnife.bind(this, mView);


        setDataonWidget();

        return mView;
    }

    private void setDataonWidget() {
        nameTv.setText(Constants.Name);
        if(!Constants.ProfilePicture.equals("")){
//            progressLL.setVisibility(View.VISIBLE);
//            Glide.with(getActivity())
//                    .load(hypnofyPreference.readString(getActivity(),hypnofyPreference.USER_IMAGE,""))
//                    .listener(new RequestListener<String, GlideDrawable>() {
//                        @Override
//                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
//                            progressLL.setVisibility(View.GONE);
//                            return false;
//                        }
//
//                        @Override
//                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
//                            progressLL.setVisibility(View.GONE);
//                            return false;
//                        }
//                    }).into( ImageViewIV);
            Glide.with(getActivity())
                    .load(hypnofyPreference.readString(getActivity(),hypnofyPreference.USER_IMAGE,""))
                    .asBitmap()
                    .placeholder(R.drawable.placeholder)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            // you can do something with loaded bitmap here
                            ImageViewIV.setImageBitmap(resource);
                        }
                    });

        }
//        nameTv.setText(hypnofyPreference.readString(getActivity(),hypnofyPreference.FIRST_NAME,"") );
//        + " " + hypnofyPreference.readString(getActivity(),hypnofyPreference.LAST_NAME,"")
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    ////    R.id.imgEditIV,
    @OnClick({R.id.logoutLL, R.id.editTv, R.id.replayLL, R.id.subscriberLL, R.id.paymentLL, R.id.sessionLL, R.id.preferLL, R.id.reminderLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.editTv:
//                Toast.makeText(getActivity(),getString(R.string.comming_soon),Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(), EditProfileActivity.class));
getActivity().finish();
                break;
            case R.id.logoutLL:
                logout();
                break;
            case R.id.replayLL:
                Toast.makeText(getActivity(), getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
            case R.id.subscriberLL:
                Toast.makeText(getActivity(), getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
            case R.id.paymentLL:
                Toast.makeText(getActivity(), getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
            case R.id.sessionLL:
                Toast.makeText(getActivity(), getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
            case R.id.preferLL:
                Toast.makeText(getActivity(), getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
            case R.id.reminderLL:
                Toast.makeText(getActivity(), getString(R.string.comming_soon), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }



    private void logout() {
        SharedPreferences preferences = hypnofyPreference.getPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(hypnofyPreference.IS_LOGIN);
        editor.remove(hypnofyPreference.USER_ID);
        editor.remove(hypnofyPreference.FIRST_NAME);
        editor.remove(hypnofyPreference.LAST_NAME);
        editor.remove(hypnofyPreference.EMAIL);
        editor.clear();
        editor.commit();
        Intent mIntent = new Intent(getActivity(), LoginActivity.class);
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(mIntent);
    }


}
