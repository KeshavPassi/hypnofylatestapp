package com.hypnofy.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hypnofy.R;
import com.hypnofy.activities.LikeHelpActivity;
import com.hypnofy.utils.Constants;
import com.hypnofy.utils.hypnofyPreference;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class HomeFragment extends Fragment {
    View mView;
    @BindView(R.id.btnLetsGO)
    RelativeLayout btnLetsGO;
    @BindView(R.id.txtUserWithGoodTV)
    TextView txtUserWithGoodTV;
    @BindView(R.id.txtUserWithHeleneTV)
    TextView txtUserWithHeleneTV;
    Unbinder unbinder;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, mView);

    txtUserWithHeleneTV.setText(hypnofyPreference.readString(getActivity(), hypnofyPreference.FIRST_NAME, "") );


        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btnLetsGO)
    public void onViewClicked() {
        startActivity(new Intent(getActivity(), LikeHelpActivity.class));
    }
}
